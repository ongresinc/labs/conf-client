package client

import (
	"encoding/json"
	"fmt"
)

// Config represents a configuration that will be stored on api.postgresqlco.nf service
type Config struct {
	// Name of the configuration
	Name string
	// Version, full PostgreSQL version
	Version float32

	// Public configuration?
	Public bool
}

type exporConfig struct {
	Name       string `json:"name"`
	Version    string `json:"pgVersion"`
	Visibility string `json:"visibility"`
}

// NewConfig creates a new valid configuration
func NewConfig(name string, version float32, public bool) *Config {
	return &Config{
		Name:    name,
		Version: version,
		Public:  public,
	}
}

func (c *Config) toJSON() (string, error) {

	var out = exporConfig{
		Name:       c.Name,
		Version:    fmt.Sprintf("%.0f", c.Version),
		Visibility: "private",
	}

	if c.Public {
		out.Visibility = "public"
	}

	bOut, err := json.Marshal(out)

	return string(bOut), err
}
