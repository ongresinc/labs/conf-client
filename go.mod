module gitlab.com/ongresinc/labs/conf-client

go 1.13

require (
	github.com/rs/zerolog v1.18.0
	github.com/spf13/pflag v1.0.5
)
