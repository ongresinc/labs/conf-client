package main

import (
	"log"

	client "gitlab.com/ongresinc/labs/conf-client"
)

func main() {
	cli := client.NewClient(config.Token)
	cli.URL = "https://kmv0ah4r3h.execute-api.us-east-1.amazonaws.com"

	newConfig := client.NewConfig("foo", 12.2, false)

	err := cli.SaveConfig(newConfig)

	if err != nil {
		log.Fatalf("could not send Config: %v", err)
	}
}
