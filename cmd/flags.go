package main

import (
	"fmt"
	"os"

	flag "github.com/spf13/pflag"
)

var (
	config struct {
		Token string
	}
)

const (
	appName = "conf-client"
)

func init() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s is a client for the postgresqlco.nf API.\n\n", appName)
		fmt.Fprintf(os.Stderr, "Usage:\n")
		fmt.Fprintf(os.Stderr, "  %s [OPTIONS]...\n\n", appName)
		fmt.Fprintf(os.Stderr, "General Options:\n")

		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nReport BUGs on https://gitlab.com/ongresinc/labs/conf-client.\n\n")
	}

	flag.StringVarP(&config.Token, "token", "T", "", "token to authenticate the API")

	flag.Parse()
}
