package client

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
)

// Setup is the client configuration
type Setup struct {
	// URL default for the conf API
	URL string

	// Token to authenticate on the API
	Token string
}

// NewClient creates a client config
func NewClient(token string) *Setup {
	return &Setup{
		Token: token,
	}
}

const configEndPoint = "/api/v1/configs"

// SaveConfig will update the config on the API
func (c *Setup) SaveConfig(conf *Config) error {

	client := &http.Client{}

	msg, err := conf.toJSON()

	if err != nil {
		return fmt.Errorf("could not convert the config to JSON: %v", err)
	}

	reqBody := strings.NewReader(msg)

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/%s", c.URL, configEndPoint), reqBody)

	if err != nil {
		return fmt.Errorf("could not create a request: %v", err)
	}

	req.Header.Add("If-None-Match", `W/"wyzzy"`)
	resp, err := client.Do(req)

	if err != nil {
		return fmt.Errorf("could not execute the request: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return fmt.Errorf("could not read the request body: %v", err)
	}

	log.Printf("result: %s", string(body))

	return nil
}
